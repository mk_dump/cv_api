
#NOTE:  This must be the first call in order to work properly!

from pathlib import Path

from deoldify import device
from deoldify.device_id import DeviceId
#choices:  CPU, GPU0...GPU7
device.set(device=DeviceId.GPU0)

from deoldify.visualize import *
plt.style.use('dark_background')
torch.backends.cudnn.benchmark=True
import warnings
warnings.filterwarnings("ignore", category=UserWarning, message=".*?Your .*? set is empty.*?")




colorizer = get_image_colorizer(artistic=False)


def colorify_image(source_path, result_path = './images_output_colorify', render_factor = 35):
    #render_factor=35
    #NOTE:  Make source_url None to just read from file at ./video/source/[file_name] directly without modification
    source_url=None
    source_path = source_path
    result_path = result_path

    

    result = colorizer.get_transformed_image(path=source_path, render_factor=render_factor )

    
    img_path = 'colored_' + Path(source_path).name

    result_path = colorizer._save_result_image(Path(img_path), result, results_dir=Path(result_path))
    result.close()

    
    return str(result_path)