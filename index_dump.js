
const fileInput = document.querySelector('#your-file-input') ;
const formData = new FormData();


formData.append('file', fileInput.files[0]);

const options = {
      method: 'POST',
      body: formData,
      // If you add this, upload won't work
      // headers: {
      //   'Content-Type': 'multipart/form-data',
      // }
};
    
//fetch('your-upload-url', options);



const imageUrl = "https://.../image.jpg";

fetch(imageUrl, options)
  //                         vvvv
  .then(response => response.blob())
  .then(imageBlob => {
      // Then create a local URL for that image and print it 
      const imageObjectURL = URL.createObjectURL(imageBlob);
      console.log(imageObjectURL);
  });